This is a simple Cucumber project that I created to learn Cucumber and Sahi Ruby driver integration. 
Running this automated test will search for the term 'Sahi' in Google and verify that google returned
more than 1000 results.

Special thanks:
  Alister Scott (http://watirmelon.com/2011/01/21/my-simple-cucumber-watir-page-object-pattern-framework/)
  Martin Fowler (http://martinfowler.com/bliki/PageObject.html)