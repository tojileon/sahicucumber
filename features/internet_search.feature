Feature: Internet Search

	Scenario Outline: Search for keyword
		Given I am on the <search engine> home page
		When I search for <keyword>
		Then I should see at least <expected number of> results
		
		Examples:
		| search engine | keyword | expected number of |
		| Google		    | "Sahi"  | 1000      				 |
    