Given(/^I am on the (.+) home page$/) do |site|
  @google_home = GoogleHomePage.new(@browser) 
end

When(/^I search for "(.*?)"$/) do |term|
  @google_result = @google_home.search(term)
end

Then(/^I should see at least (\d+) results$/) do |n|
  @google_result.result_stats.should >= n.to_i
end
