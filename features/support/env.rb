require 'sahi'
require 'rspec'

browser = Sahi::Browser.new(ENV["BROWSER"])
browser.open

Before do
  @browser = browser
end