class GoogleResultPage
  attr_reader :result_stats
  
  def initialize(browser)
    @browser = browser
    @result_stats = @browser.div(id="resultStats").text.scan(/About (\d.*) results/)[0][0].delete(',').to_i
  end
end