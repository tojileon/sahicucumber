class GoogleHomePage
  def initialize(browser)
    @browser = browser
    @browser.navigate_to("http://www.google.com/webhp?nord=1")
    @search_input = @browser.textbox(id="q")
    @search_button = @browser.span("gbqfi gb_Aa")
  end
  
  def search(term)
    @search_input.value = term 
    @search_button.click
    GoogleResultPage.new(@browser)
  end
end